# README #

[Interacting with Physically-Based character using Motion Capture with Kinect Sensor ](https://www.youtube.com/watch?v=sRssPNgBHw8)

### What is this repository for? ###

* The use of animated characters is one of the main factors of successful games and simulators. Characters that react to the environment around them provide greater immersion to the user. One way to improve the user immersion is to provide greater control over the character. Thinking about it, a physically simulated character prototype created based on the physical characteristics of the user. Thus the character used in the simulation becomes unique for each user, seeking to improve the user immersion to have a character that represents him, with their advantages and physical limitations. A Kinect Sensor was used in order to capture user movement in conjunction with the physical simulation library Open Dynamics Engine, responsible for creating the bodies that make up the character, and calculating the torques and forces necessary to animate the character through a PD controller. The OpenGL library was used to design the simulation elements. Once created the character, the same is inserted into interaction scenarios previously configured to analyze user interaction. The prototype represented with a satisfactory
level of accuracy the user’s movements and reactions to external perturbations from the virtual environment. The configured scenarios and the interface designed to control the simulation were effective in the tests, proving that the characters created from this work have characteristics of users who used it.
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact