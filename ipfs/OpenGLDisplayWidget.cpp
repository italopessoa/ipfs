#include "OpenGLDisplayWidget.h"
#include <QtWidgets>
#include <QtOpenGL>
#include <QDebug>
#include <QMutex>

//#include "Sim/Kinect.h"

OpenGLDisplayWidget::OpenGLDisplayWidget(QWidget *parent)
    : QGLWidget(QGLFormat(QGL::SampleBuffers),parent)
{
    simulation = new ODESimulation();
    k = new Kinect(this);


    connect(k,SIGNAL(SkeletonChanged(QList<XnSkeletonJointTransformation*>*)),
            simulation,SLOT(onSkeletonChanged(QList<XnSkeletonJointTransformation*>*)));
}

OpenGLDisplayWidget::~OpenGLDisplayWidget()
{
    StopKinect();
}

void OpenGLDisplayWidget::StartKinect()
{
    k->Setup();
    k->start();
}

void OpenGLDisplayWidget::PauseKinect()
{
    k->Pause();
}

void OpenGLDisplayWidget::StopKinect()
{
    QMutex mutex;
    mutex.lock();
    if(k->isRunning())
    {
        k->Stop();
        k->terminate();
    }
    mutex.unlock();
}

void OpenGLDisplayWidget::ResumeKinect()
{
    k->Resume();
}

void OpenGLDisplayWidget::initializeGL()
{
    //k->Setup();

    simulation->Start();
    qglClearColor(Qt::black);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_CULL_FACE);
    glShadeModel(GL_SMOOTH);
    this->ConfigLights();

    principalCamera = new CameraDistante(0, 10, 70, 0, 0, 0, 0, 1, 0);

    //k->start();
    //k->start();
    //k->run();
    //k->Teste();
}

void OpenGLDisplayWidget::paintGL()
{
    DisplayInit();
    simulation->ExecuteLoop();
    this->Draw();
    update();
    //DisplayEnd();


    //http://stackoverflow.com/questions/15002045/how-to-force-qglwidget-to-update-screen
    //update();//isso substitui o idle, mas acho que pode comprometer o desempenho
}

void OpenGLDisplayWidget::resizeGL(int width, int height)
{
    //como o DisplayInit() ja utiliza o valor atual da tela
    //nao precisa adicionar nada aqui
}

void OpenGLDisplayWidget::mousePressEvent(QMouseEvent *event)
{
    //lastPos = event->pos();
    //qCritical()<<"fatal";

    cameraLastX = event->x();
    cameraLastY = event->y();
    updateGL();
}

void OpenGLDisplayWidget::mouseMoveEvent(QMouseEvent *event)
{
    //http://stackoverflow.com/questions/20116772/how-to-detect-if-both-mouse-buttons-are-pressed-in-qt
    if((event->buttons() & Qt::RightButton) &&
        (event->buttons() & Qt::LeftButton))
    {
         principalCamera->zoom(event->y(), cameraLastY);
    }
    else if(event->buttons() & Qt::LeftButton)
    {
        principalCamera->rotateX(event->y(), cameraLastY);
        principalCamera->rotateY(event->x(), cameraLastX);
    }
    else if(event->buttons() & Qt::RightButton)
    {
        principalCamera->translateX(event->x(), cameraLastX);
        principalCamera->translateY(event->y(), cameraLastY);
    }

    //TODO: mover para dentro da class
    cameraLastX = event->x();
    cameraLastY = event->y();
    updateGL();//glutPostRedisplay();
}

void OpenGLDisplayWidget::ConfigLights()
{
    glEnable(GL_LIGHTING);
    glEnable(GL_COLOR_MATERIAL);
    glEnable(GL_NORMALIZE);

    glEnable(GL_LIGHT0);

    const GLfloat light_ambient[] = { 0.5f, 0.5f, 0.5f, 1.0f };
    const GLfloat light_diffuse[] = { 1.0f, 1.0f, 1.0f, 1.0f };
    const GLfloat light_specular[] = { 1.0f, 1.0f, 1.0f, 1.0f };
    const GLfloat light_position[] = { 2.0f, 5.0f, 5.0f, 0.0f };

    const GLfloat mat_ambient[] = { 0.7f, 0.7f, 0.7f, 1.0f };
    const GLfloat mat_diffuse[] = { 0.8f, 0.8f, 0.8f, 1.0f };
    const GLfloat mat_specular[] = { 1.0f, 1.0f, 1.0f, 1.0f };
    const GLfloat high_shininess[] = { 100.0f };

    glLightfv(GL_LIGHT0, GL_AMBIENT, light_ambient);
    glLightfv(GL_LIGHT0, GL_DIFFUSE, light_diffuse);
    glLightfv(GL_LIGHT0, GL_SPECULAR, light_specular);
    glLightfv(GL_LIGHT0, GL_POSITION, light_position);

    glMaterialfv(GL_FRONT, GL_AMBIENT, mat_ambient);
    glMaterialfv(GL_FRONT, GL_DIFFUSE, mat_diffuse);
    glMaterialfv(GL_FRONT, GL_SPECULAR, mat_specular);
    glMaterialfv(GL_FRONT, GL_SHININESS, high_shininess);
}

void OpenGLDisplayWidget::DisplayInit()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glViewport(0, 0, this->width(), this->height());

    const float ar = this->height() > 0 ? (float)this->width() / (float)this->height() : 1.0;

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(30., ar, 0.1, 1000.);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    /*gluLookAt(principalCamera->e.x,principalCamera->e.y,principalCamera->e.z,
              principalCamera->c.x,principalCamera->c.y,principalCamera->c.z,
              principalCamera->u.x,principalCamera->u.y,principalCamera->u.z);
    */
    principalCamera->updateCameraGL();
    //gluLookAt(0, 10, 70, 0, 0, 0, 0, 1, 0);
}

void OpenGLDisplayWidget::DisplayEnd()
{
    glutSwapBuffers();
}

void OpenGLDisplayWidget::Draw()
{
    GLfloat black[3] = {0,0,0};
    GLfloat white[3] = {1,1,1};
    Desenha::drawChessFloor(100,0.03,black,white);
}

Kinect* OpenGLDisplayWidget::getKinect()
{
    return k;
}

ODESimulation* OpenGLDisplayWidget::getSimulation()
{
    return simulation;
}
