#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QWidget>
#include <XnTypes.h>
#include "Tools/Utils/Structs.h"
#include <QHash>

namespace Ui {
class MainWindow;
}

class MainWindow : public QWidget
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

public slots:


private slots:
    void on_btnStartKinect_clicked();

    void on_btnPauseKinect_clicked();

    void on_NewUserDetected(int userID);

    void on_UserLost(int userID);

    void on_spinMaxModels_valueChanged(int arg1);

    void on_btnUserCalibrate_clicked();

    void on_SkeletonChanged(QList<XnSkeletonJointTransformation*>* skeletons);

    void on_btnInsertModel_clicked();

signals:
    void MaxModelNumberChange(int max);

    void HumanoidCalibration(SkeletonJointPosition *skeletonJoints, int modelID);

    void InsertModel(int modelID);

private:
    Ui::MainWindow *ui;
    QHash<int, SkeletonJointPosition*> skeletonJointsHash;

};

#endif // MAINWINDOW_H
