#ifndef OPENGLDISPLAYWIDGET_H
#define OPENGLDISPLAYWIDGET_H

#include <QGLWidget>
#include <GL/glu.h>
#include <GL/glut.h>
#include <Tools/Desenha.h>
#include <Tools/CameraDistante.h>
#include "Sim/ODESimulation.h"
#include "Sim/Kinect.h"

class OpenGLDisplayWidget : public QGLWidget
{
    Q_OBJECT
public:
    explicit OpenGLDisplayWidget(QWidget *parent = 0);
    ~OpenGLDisplayWidget();

    void StartKinect();

    void PauseKinect();

    void StopKinect();

    void ResumeKinect();

    Kinect* getKinect();

    ODESimulation* getSimulation();

signals:

public slots:

private:
    /**
     * @brief SetupLights
     */
    void ConfigLights();

    /**
     * @brief Draw
     */
    void Draw();

    /**
     * @brief DisplayInit
     */
    void DisplayInit();

    /**
     * @brief DisplayEnd
     */
    void DisplayEnd();

    /**
     * @brief principalCamera
     */
    Camera *principalCamera;

    //TODO: utilizar para movimentar a camera, remove da main
    GLfloat cameraLastX = 0.0;
    GLfloat cameraLastY = 0.0;

    ODESimulation *simulation;

    Kinect *k;
protected:


    /**
     * @brief initializeGL will be called to initialize the GL Context.
     */
    void initializeGL();

    /**
     * @brief paintGL will be called when the OpenGL widget needs to be redrawn
     */
    void paintGL();

    /**
     * @brief resizeGL will be called when the widget is resized
     * @param width The widget's width
     * @param height The widget's height
     */
    void resizeGL(int width, int height);

    /**
     * @brief mousePressEvent will be called when the user execute a click on the OpenGL screen
     * @param event The click event
     */
    void mousePressEvent(QMouseEvent *event);

    /**
     * @brief mouseMoveEvent will be called when the mouse was moved around the OpenGL screen
     * @param event Mouse events, clicks, press, position
     */
    void mouseMoveEvent(QMouseEvent *event);

};

#endif // OPENGLDISPLAYWIDGET_H
