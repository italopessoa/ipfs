#ifndef CAMERA_H
#define CAMERA_H

#define CAMDIST 1
#define CAMJOGO 2

#include <math.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include "gVector3D.h"

class Camera
{
   public:

      gVector3D<GLfloat> e;
      gVector3D<GLfloat> c;
      gVector3D<GLfloat> u;

      int estilo;

      Camera();
      Camera(gVector3D<GLfloat> e, gVector3D<GLfloat> c, gVector3D<GLfloat> u);
      Camera(GLfloat ex, GLfloat ey, GLfloat ez, GLfloat cx, GLfloat cy, GLfloat cz, GLfloat ux, GLfloat uy, GLfloat uz);
      ~Camera(){};

      //vetor Vec e u sempre perpendiculares e u sempre unitario
      /**
       * @brief zoom
       * @param actualY
       * @param lastY
       */
      virtual void zoom(GLfloat actualY, GLfloat lastY) = 0;

      /**
       * @brief translateX
       * @param actualX
       * @param lastX
       */
      virtual void translateX(GLfloat actualX, GLfloat lastX) = 0;

      /**
       * @brief translateY
       * @param actualY
       * @param lastY
       */
      virtual void translateY(GLfloat actualY, GLfloat lastY) = 0;

      /**
       * @brief rotateX
       * @param actualX
       * @param lastX
       */
      virtual void rotateX(GLfloat actualX, GLfloat lastX) = 0;

      /**
       * @brief rotateY
       * @param actualY
       * @param lastY
       */
      virtual void rotateY(GLfloat actualY, GLfloat lastY) = 0;

      /**
       * @brief rotateZ
       * @param actualZ
       * @param lastZ
       */
      virtual void rotateZ(GLfloat actualZ, GLfloat lastZ) = 0;

      /**
       * @brief updateCameraGL
       */
      virtual void updateCameraGL() = 0;

      virtual gVector3D<GLfloat> getPickedPoint(GLfloat, GLfloat) = 0;
      void adaptavetorcdisttojogo();
      void adaptavetorcjogotodist(GLfloat r);
};

#endif
