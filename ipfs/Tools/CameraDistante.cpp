#include "CameraDistante.h"
//---------------------------------------------------------------------------
CameraDistante::CameraDistante() : Camera()
{
  c.z=0;

  estilo = CAMDIST;
}
//---------------------------------------------------------------------------
CameraDistante::CameraDistante(gVector3D<GLfloat> e, gVector3D<GLfloat> c, gVector3D<GLfloat> u) :
      Camera(e, c, u)
{
  estilo = CAMDIST;
}
//---------------------------------------------------------------------------
CameraDistante::CameraDistante(GLfloat ex, GLfloat ey, GLfloat ez, GLfloat cx, GLfloat cy, GLfloat cz, GLfloat ux, GLfloat uy, GLfloat uz) :
      Camera(ex, ey, ez, cx, cy, cz, ux, uy, uz)
{
  estilo = CAMDIST;
}
//---------------------------------------------------------------------------
void CameraDistante::zoom(GLfloat win_y, GLfloat last_y){
  //vetor do olho(eye) ao centro(center)
  gVector3D<GLfloat> Vec = c.subtracao(e);
  //normalizando o Vec
  Vec.normaliza();

  //GLfloat moduloVec = Vec.modulo();
  //if ((moduloVec > 1) || (last_y > win_y)){
  //  e = e.soma(Vec.multiplicacao( (win_y - last_y)/2 ));
  //}
  //nova estrategia bem melhor
  gVector3D<GLfloat> eNovo = e.soma(Vec.multiplicacao( (win_y - last_y) ));
  if ( c.subtracao(eNovo).prodEscalar(Vec) >= 0.0001 ){
    e = eNovo;
  }
}
//---------------------------------------------------------------------------
void CameraDistante::translateX(GLfloat win_x, GLfloat last_x){
  //vetor do olho(eye) ao centro(center)
  gVector3D<GLfloat> Vec = c.subtracao(e);
  //vetor no sentido positivo da direcao x
  gVector3D<GLfloat> Xpos = Vec.prodVetorial(u);
  Xpos.normaliza();

  e = e.soma( Xpos.multiplicacao( Vec.modulo()*(last_x - win_x)/300.0 ) );
  c = c.soma( Xpos.multiplicacao( Vec.modulo()*(last_x - win_x)/300.0 ) );
}
//---------------------------------------------------------------------------
void CameraDistante::translateY(GLfloat win_y, GLfloat last_y){
  //vetor do olho(eye) ao centro(center)
  gVector3D<GLfloat> Vec = c.subtracao(e);

  e = e.subtracao( u.multiplicacao( Vec.modulo()*(last_y - win_y)/300.0 ) );
  c = c.subtracao( u.multiplicacao( Vec.modulo()*(last_y - win_y)/300.0 ) );
}
//---------------------------------------------------------------------------
void CameraDistante::rotateX(GLfloat win_y, GLfloat last_y){
  //vetor auxiliar aux
  gVector3D<GLfloat> aux = c;//gVector3D<GLfloat>(c.x, c.y, c.z);
  c = c.multiplicacao(2.0).subtracao(e);
  e = aux;//.recebe(aux);

  //vetor do olho(eye) ao centro(center)
  gVector3D<GLfloat> Vec = c.subtracao(e);
  GLfloat moduloVecant = Vec.modulo();
  c = c.soma(u.multiplicacao( ((1.0/30.0)*moduloVecant)*(last_y - win_y)/5.0 ));
  Vec = c.subtracao(e);
  GLfloat moduloVec = Vec.modulo();
    Vec = Vec.multiplicacao( moduloVecant/moduloVec );
  c = e.soma(Vec);
  Vec = c.subtracao(e);

  //vetor no sentido positivo da direcao x
  gVector3D<GLfloat> Xpos = Vec.prodVetorial(u);
  u = Xpos.prodVetorial(Vec);
  u.normaliza();

  aux = e;//.recebe(e);
  e = e.multiplicacao(2.0).subtracao(c);
  c = aux;//.recebe(aux);
}
//---------------------------------------------------------------------------
void CameraDistante::rotateY(GLfloat win_x, GLfloat last_x){
  //vetor auxiliar aux
  gVector3D<GLfloat> aux = c;//gVector3D<GLfloat>(c.x, c.y, c.z);
  c = (c*2)-e;
  e = aux;//.recebe(aux);

  //vetor do olho(eye) ao centro(center)
  gVector3D<GLfloat> Vec = c-e;
  GLfloat moduloVecant = Vec.modulo();
  //vetor no sentido positivo da direcao x
  gVector3D<GLfloat> Xpos = Vec.prodVetorial(u);
  Xpos.normaliza();

  c = c.subtracao(Xpos*( ((1.0/30.0)*moduloVecant)*(last_x - win_x)/5.0 ));

  Vec = c-e;
  GLfloat moduloVec = Vec.modulo();
    Vec = Vec*( moduloVecant/moduloVec );
  c = e+Vec;

  //novo-----------------------------------
  //atualizando u
  gVector3D<GLfloat> up;
  if (u.y>=0.0) {
    up = gVector3D<GLfloat>(0.0,1.0,0.0);
  } else {
    up = gVector3D<GLfloat>(0.0,-1.0,0.0);
  }
  gVector3D<GLfloat> XposUp = Vec.prodVetorial(up);
  u = XposUp.prodVetorial(Vec);
  u.normaliza();
  //fim_novo-------------------------------

  aux = e;//.recebe(e);
  e = e.multiplicacao(2.0).subtracao(c);
  c = aux;//.recebe(aux);
}
//---------------------------------------------------------------------------
void CameraDistante::rotateZ(GLfloat win_x, GLfloat last_x){
  //vetor do olho(eye) ao centro(center)
  gVector3D<GLfloat> Vec = c.subtracao(e);
  //vetor no sentido positivo da direcao x
  gVector3D<GLfloat> Xpos = Vec.prodVetorial(u);
  Xpos.normaliza();

  //modificando o vetor up
  u = u.soma(Xpos.multiplicacao( (last_x - win_x)/300.0 ));
  u.normaliza();
}
//---------------------------------------------------------------------------
//passando o ponto local a camera (x,y,-1) para as coordenadas do mundo
gVector3D<GLfloat> CameraDistante::getPickedPoint(GLfloat x, GLfloat y){
  //calculando a base da camera
  //vetor do centro(center) ao olho(eye) - Zpos
  gVector3D<GLfloat> Vce = e.subtracao(c);
  Vce.normaliza();
  //vetor no sentido positivo da direcao x
  gVector3D<GLfloat> Xpos = u.prodVetorial(Vce);
  Xpos.normaliza();

  //mudanca da base da camera para a base do mundo (canonica)
  float dx = Xpos.x * x + u.x * y + Vce.x * -1;
  float dy = Xpos.y * x + u.y * y + Vce.y * -1;
  float dz = Xpos.z * x + u.z * y + Vce.z * -1;

  //translacao em relacao a posicao da camera
  dx += e.x;
  dy += e.y;
  dz += e.z;

  return gVector3D<GLfloat>(dx,dy,dz);
}

void CameraDistante::updateCameraGL()
{
    gluLookAt(e.x,e.y,e.z,c.x,c.y,c.z,u.x,u.y,u.z);
}

//---------------------------------------------------------------------------
