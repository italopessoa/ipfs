#include "Color.h"

Color::Color()
{
    r = g = b = 0.0;
}

Color::~Color()
{

}

Color::Color(GLfloat r, GLfloat g, GLfloat b)
{
    this->r = r;
    this->g = g;
    this->b = b;
}

void Color::setR(GLfloat r)
{
    this->r = r;
    colorv[0] = r;
}

GLfloat Color::getR()
{
    return this->r;
}

void Color::setG(GLfloat g)
{
    this->g = g;
    colorv[1] = g;
}

GLfloat Color::getG()
{
    return this->g;
}

void Color::setB(GLfloat b)
{
    this->b = b;
    colorv[2] = b;
}

GLfloat Color::getB()
{
    return this->b;
}

GLfloat* Color::getColorv()
{
    return colorv;
}

