#ifndef STRUCTS
#define STRUCTS

#include <ode/common.h>
#include "Tools/gVector3D.h"
#include <XnTypes.h>

typedef struct SkeletonJointPosition
{
    //TODO: verificar se é realmente apenas 0 e 1
    gVector3D<dReal> parentPosition;
    bool parentPositionConfidence;

    gVector3D<dReal> childPosition;
    bool childPositionConfidence;

    gVector3D<dReal> position;
    bool positionConfidence;

    XnMatrix3X3 orientation;
    bool orientationConfidence;
} SkeletonJointPosition;

#endif // STRUCTS

