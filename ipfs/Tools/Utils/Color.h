#ifndef COLOR_H
#define COLOR_H
#include <GL/gl.h>

class Color
{
public:
    /**
     * @brief Color
     */
    Color();

    /**
     * @brief Color
     * @param r
     * @param g
     * @param b
     */
    Color(GLfloat r, GLfloat g, GLfloat b);

    /**
     * @brief setR
     * @param r
     */
    void setR(GLfloat r);

    /**
     * @brief getR
     * @return
     */
    GLfloat getR();

    /**
     * @brief setG
     * @param g
     */
    void setG(GLfloat g);

    /**
     * @brief getG
     * @return
     */
    GLfloat getG();

    /**
     * @brief setB
     * @param b
     */
    void setB(GLfloat b);

    /**
     * @brief getB
     * @return
     */
    GLfloat getB();

    /**
     * @brief getColorv
     * @return
     */
    GLfloat* getColorv();

    ~Color();
private:

    /**
     * @brief r
     */
    GLfloat r;

    /**
     * @brief g
     */
    GLfloat g;

    /**
     * @brief b
     */
    GLfloat b;

    /**
     * @brief colorv
     */
    GLfloat colorv[3];
};

#endif // COLOR_H
