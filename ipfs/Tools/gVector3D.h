#ifndef GVECTOR3D_H
//#define GVECTOR3D_H
//o define foi movido para o fim do arquivo para evitar erros
//ao utilizar mais de dois tipos dentro de uma mesma classe
//para entender melhor é só remover esse comentario e comentar no final do arquivo


template<typename T>
class gVector3D
{
public:

    gVector3D();
    gVector3D(T x, T y, T z);
    T x;
    T y;
    T z;

   ~gVector3D();

    void setVetor3D(T x, T y, T z);
    T modulo();
    T modulo2(); //quadrado do modulo (nao tira a raiz quadrada)
    void normaliza();
    gVector3D getUnit();
    gVector3D projectedOn(gVector3D v);
    void recebe(gVector3D v);
    gVector3D soma(gVector3D v);
    gVector3D operator+(gVector3D v);

    void add(gVector3D v);

    gVector3D subtracao(gVector3D v);
    gVector3D operator-(gVector3D v);

    gVector3D multiplicacao(T escalar);
    gVector3D operator*(T rhs);
    //Vetor3D operator*(gVector3D lhs, const T escalar);
    // Fraction& operator*=(const Fraction& rhs)
    T getDistance(gVector3D v);
    gVector3D prodVetorial(gVector3D v);
    T prodEscalar(gVector3D v);
};


template<typename T>
gVector3D<T>::gVector3D()
{
    this->x = 0.0;
    this->y = 0.0;
    this->z = 0.0;
}

template<typename T>
gVector3D<T>::gVector3D(T x, T y, T z)
{
    this->x = x;
    this->y = y;
    this->z = z;
}

//Destrutor
template<typename T>
gVector3D<T>::~gVector3D()
{
}
//---------------------------------------------------------------------------
//atualiza o vetor
template<typename T>
void gVector3D<T>::setVetor3D(T x, T y, T z) {
    this->x = x;
    this->y = y;
    this->z = z;
}
//---------------------------------------------------------------------------
//retorna o modulo do vetor
template<typename T>
T gVector3D<T>::modulo() {
    return sqrt(this->x * this->x + this->y * this->y + this->z * this->z);
}
//---------------------------------------------------------------------------
//retorna o quadrado do modulo do vetor (nao tira a raiz quadrada)
template<typename T>
T gVector3D<T>::modulo2() {
    return (this->x * this->x + this->y * this->y + this->z * this->z);
}
//---------------------------------------------------------------------------
//normaliza o vetor
template<typename T>
void gVector3D<T>::normaliza() {
    T m = modulo();

    if (m > 0.0) {
        m = 1 / m;

        this->x *= m; // X
        this->y *= m; // Y
        this->z *= m; // Z
    }
}
//---------------------------------------------------------------------------
template<typename T>
gVector3D<T> gVector3D<T>::getUnit() {
    gVector3D u = *this;
    u.normaliza();

    return u;
}
//---------------------------------------------------------------------------
template<typename T>
gVector3D<T> gVector3D<T>::projectedOn(gVector3D v) {
    gVector3D v_u = v.getUnit();
    gVector3D projection = v_u.multiplicacao( this->prodEscalar( v_u ) );

    return projection;
}
//---------------------------------------------------------------------------
//atribuicao
template<typename T>
void gVector3D<T>::recebe(gVector3D v) {
    this->x = v.x;
    this->y = v.y;
    this->z = v.z;
}
//---------------------------------------------------------------------------
//soma
template<typename T>
gVector3D<T> gVector3D<T>::soma(gVector3D v) {
    return gVector3D(
                this->x + v.x,
                this->y + v.y,
                this->z + v.z
                );
}

template<typename T>
gVector3D<T> gVector3D<T>::operator +(gVector3D v) {
    return gVector3D(
                this->x + v.x,
                this->y + v.y,
                this->z + v.z
                );
}
//---------------------------------------------------------------------------
//add - adiciona
template<typename T>
void gVector3D<T>::add(gVector3D v) {
    this->x += v.x;
    this->y += v.y;
    this->z += v.z;
}
//---------------------------------------------------------------------------
//subtracao
template<typename T>
gVector3D<T> gVector3D<T>::subtracao(gVector3D v) {
    return gVector3D(
                this->x - v.x,
                this->y - v.y,
                this->z - v.z
                );
}
template<typename T>
gVector3D<T> gVector3D<T>::operator-(gVector3D v)
{
    return gVector3D(
                this->x - v.x,
                this->y - v.y,
                this->z - v.z
                );
}

//---------------------------------------------------------------------------
//multiplicacao
template<typename T>
gVector3D<T> gVector3D<T>::multiplicacao(T escalar) {
    return gVector3D(
                this->x * escalar,
                this->y * escalar,
                this->z * escalar
                );
}

template<typename T>
gVector3D<T> gVector3D<T>::operator *(T escalar)
{
    return gVector3D(
                this->x * escalar,
                this->y * escalar,
                this->z * escalar
                );
}

//---------------------------------------------------------------------------
//distancia
template<typename T>
T gVector3D<T>::getDistance(gVector3D v) {
    return subtracao(v).modulo();
}
//---------------------------------------------------------------------------
//produto vetorial
template<typename T>
gVector3D<T> gVector3D<T>::prodVetorial(gVector3D v) {
    return gVector3D(
                this->y * v.z - this->z * v.y,
                this->z * v.x - this->x * v.z,
                this->x * v.y - this->y * v.x
                );
}
//---------------------------------------------------------------------------
//produto escalar
template<typename T>
T gVector3D<T>::prodEscalar(gVector3D v) {
    return (this->x * v.x + this->y * v.y + this->z * v.z);
}
#define GVECTOR3D_H
#endif // GVECTOR3D_H
