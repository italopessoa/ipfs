#ifndef CAMERADISTANTE_H
#define CAMERADISTANTE_H

#include <math.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include "Camera.h"

class CameraDistante : public Camera
{
   public:

      CameraDistante();
      CameraDistante(gVector3D<GLfloat> e, gVector3D<GLfloat> c, gVector3D<GLfloat> u);
      CameraDistante(GLfloat ex, GLfloat ey, GLfloat ez, GLfloat cx, GLfloat cy, GLfloat cz, GLfloat ux, GLfloat uy, GLfloat uz);
      ~CameraDistante(){};

      //vetor Vec e u sempre perpendiculares e u sempre unitario
      /**
       * @brief zoom
       * @param actualY
       * @param lastY
       */
      virtual void zoom(GLfloat actualY, GLfloat lastY);

      /**
       * @brief translateX
       * @param actualX
       * @param lastX
       */
      virtual void translateX(GLfloat actualX, GLfloat lastX);

      /**
       * @brief translateY
       * @param actualY
       * @param lastY
       */
      virtual void translateY(GLfloat actualY, GLfloat lastY);

      /**
       * @brief rotateX
       * @param actualX
       * @param lastX
       */
      virtual void rotateX(GLfloat actualX, GLfloat lastX);

      /**
       * @brief rotateY
       * @param actualY
       * @param lastY
       */
      virtual void rotateY(GLfloat actualY, GLfloat lastY);

      /**
       * @brief rotateZ
       * @param actualZ
       * @param lastZ
       */
      virtual void rotateZ(GLfloat actualZ, GLfloat lastZ);

      /**
       * @brief updateCameraGL
       */
      virtual void updateCameraGL();

      virtual gVector3D<GLfloat> getPickedPoint(GLfloat, GLfloat);

};

#endif
