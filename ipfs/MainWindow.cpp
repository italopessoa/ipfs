#include "MainWindow.h"
#include "ui_MainWindow.h"
#include <QDebug>
#include <QMessageBox>

MainWindow::MainWindow(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->openGLDisplayWidget->resize(this->width(),this->height());
    ///ui->openGLDisplayWidget->updateGL();

    //http://stackoverflow.com/questions/14582591/border-of-qgroupbox
    this->setStyleSheet("QGroupBox {border: 1px solid gray;border-radius: 9px;margin-top: 0.5em;}"
                        "QGroupBox::title {subcontrol-origin: margin;left: 10px;padding: 0 3px 0 3px;}");

    ui->btnPauseKinect->setEnabled(false);
    ui->lblExecute->setVisible(false);
    ui->lblExecute->setStyleSheet("QLabel { color : red; }");

    connect(ui->openGLDisplayWidget->getKinect(),SIGNAL(NewUserDetected(int)),this,SLOT(on_NewUserDetected(int)));
    connect(ui->openGLDisplayWidget->getKinect(),SIGNAL(UserLost(int)),this,SLOT(on_UserLost(int)));
    connect(this,SIGNAL(MaxModelNumberChange(int)),ui->openGLDisplayWidget->getKinect(),SLOT(on_MaxModelNumberChanged(int)));
    connect(this,SIGNAL(MaxModelNumberChange(int)),ui->openGLDisplayWidget->getSimulation(),SLOT(on_MaxModelsNumber_Changed(int)));
    connect(this,SIGNAL(HumanoidCalibration(SkeletonJointPosition*,int)),ui->openGLDisplayWidget->getSimulation(),SLOT(on_humanoid_calibrated(SkeletonJointPosition*,int)));
    connect(this,SIGNAL(InsertModel(int)),ui->openGLDisplayWidget->getSimulation(),SLOT(on_InsertModel(int)));

    emit MaxModelNumberChange(-1);//initialize without models
}

void MainWindow::on_NewUserDetected(int userID)
{
    QString user = "User ";
    user.append(QString("%1").arg(userID));

    ui->cmbUsers->addItem(user,QVariant(userID));
}

void MainWindow::on_UserLost(int userID)
{
    QString user = "User ";
    user.append(QString("%1").arg(userID));

    int index = ui->cmbUsers->findText(user);
    if(index == -1)
    {
        qDebug()<< "The user do not exists";
    }
    else
        ui->cmbUsers->removeItem(index);

    //TODO: mover para o local adequado
    //QVariant variant = ui->cmbUsers->itemData(ui->cmbUsers->currentIndex());
    //int discRecorder = variant.value<int>();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_btnStartKinect_clicked()
{
    //TODO: adicionar negrito
    if(ui->btnStartKinect->text() == "Start")
    {
        ui->openGLDisplayWidget->StartKinect();
        ui->btnStartKinect->setEnabled(false);
        ui->btnStartKinect->setText("Stop");
        ui->btnPauseKinect->setEnabled(true);
        ui->lblExecute->setVisible(true);
        ui->lblExecute->setText("Running");
        ui->btnStartKinect->setEnabled(true);
    }
    else
    {
        ui->openGLDisplayWidget->StopKinect();
        ui->btnStartKinect->setText("Start");
        ui->btnPauseKinect->setText("Pause");
        ui->btnPauseKinect->setEnabled(false);

        ui->lblExecute->setVisible(false);
    }
}

void MainWindow::on_btnPauseKinect_clicked()
{
    //QMessageBox::information(this, "Item Selection","asdadasdasdasd");
    if(ui->btnPauseKinect->isEnabled())
    {
        if(ui->btnPauseKinect->text() == "Resume")
        {
            ui->openGLDisplayWidget->ResumeKinect();
            ui->btnPauseKinect->setText("Pause");
            ui->lblExecute->setText("Running");
        }
        else
        {
            ui->openGLDisplayWidget->PauseKinect();
            ui->btnPauseKinect->setText("Resume");
            ui->lblExecute->setText("Paused");
        }
    }
}

void MainWindow::on_spinMaxModels_valueChanged(int arg1)
{
    if(arg1 == 0) emit MaxModelNumberChange(-1);
    else emit MaxModelNumberChange(arg1);
}

void MainWindow::on_btnUserCalibrate_clicked()
{
    if(ui->spinMaxModels->value() > 0 && ui->cmbUsers->count())
    {
        connect(ui->openGLDisplayWidget->getKinect(),SIGNAL(SkeletonChanged(QList<XnSkeletonJointTransformation*>*)),
                this,SLOT(on_SkeletonChanged(QList<XnSkeletonJointTransformation*>*)));
    }
    else
    {
        QMessageBox::warning(this, "Calibrate model","You must select a model!");
    }
}

void MainWindow::on_SkeletonChanged(QList<XnSkeletonJointTransformation *> *skeletons)
{
    if(skeletons && skeletons->size() > 0)
    {
        //posso utlizar diretamente pois os elementos vem na ordem correta
        //utilizo o modulo (%) pra garantir que o objeto vai estar na posicao
        //correta de acordo com a quantidade maxima de usuarios, informada na janela
        disconnect(ui->openGLDisplayWidget->getKinect(),SIGNAL(SkeletonChanged(QList<XnSkeletonJointTransformation*>*)),
                   this,SLOT(on_SkeletonChanged(QList<XnSkeletonJointTransformation*>*)));
        QVariant selectedIndex = ui->cmbUsers->itemData(ui->cmbUsers->currentIndex());

        //TODO: melhorar a verificacao
        int model = selectedIndex.value<int>() % ui->spinMaxModels->value();

        if(skeletonJointsHash.contains(model))
        {

        }
        else
        {
            SkeletonJointPosition *skeletonJointsTemp = new SkeletonJointPosition[25];

            XnSkeletonJointTransformation *skeletonTemp = skeletons->at(model);

            SkeletonJointPosition rElbow;
            XnVector3D vectorTemp = skeletonTemp[XN_SKEL_RIGHT_ELBOW].position.position;

            rElbow.position = gVector3D<dReal>(vectorTemp.X,vectorTemp.Y,vectorTemp.Z);
            rElbow.positionConfidence = skeletonTemp[XN_SKEL_RIGHT_ELBOW].position.fConfidence;

            /*vectorTemp = skeletonTemp[XN_SKEL_RIGHT_SHOULDER].position.position;
            rElbow.parentPosition = gVector3D<dReal>(vectorTemp.X,vectorTemp.Y,vectorTemp.Z);
            rElbow.parentPositionConfidence = skeletonTemp[XN_SKEL_RIGHT_SHOULDER].position.fConfidence;*/

            rElbow.orientation = skeletonTemp[XN_SKEL_RIGHT_ELBOW].orientation.orientation;
            rElbow.orientationConfidence = skeletonTemp[XN_SKEL_RIGHT_ELBOW].orientation.fConfidence;

            vectorTemp = skeletonTemp[XN_SKEL_RIGHT_HAND].position.position;
            rElbow.childPosition = gVector3D<dReal>(vectorTemp.X,vectorTemp.Y,vectorTemp.Z);
            rElbow.childPositionConfidence = skeletonTemp[XN_SKEL_RIGHT_HAND].position.fConfidence;

            skeletonJointsTemp[XN_SKEL_RIGHT_SHOULDER] = rElbow;
            skeletonJointsHash.insert( model,skeletonJointsTemp);

            //TODO: emitir sinal

            QString msg;
            msg.append("X: ").append(QString("%1\n").arg(rElbow.position.x));
            msg.append("Y: ").append(QString("%1\n").arg(rElbow.position.y));
            msg.append("Z: ").append(QString("%1\n").arg(rElbow.position.z));

            emit HumanoidCalibration(skeletonJointsTemp,model);
            QMessageBox::warning(this, "Calibrate model",msg);
        }

    }
}

void MainWindow::on_btnInsertModel_clicked()
{
    QVariant selectedIndex = ui->cmbUsers->itemData(ui->cmbUsers->currentIndex());
    int model = selectedIndex.value<int>() % ui->spinMaxModels->value();
    emit InsertModel(model);
}
