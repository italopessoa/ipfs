#-------------------------------------------------
#
# Project created by QtCreator 2015-07-05T15:52:04
#
#-------------------------------------------------

QT       += core gui opengl

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = ipfs
TEMPLATE = app
INCLUDEPATH+=/usr/include/ni

LIBS += -lGL -lGLU -lglut -lOpenNI -lode # -lode -lX11 -lpthread -lglui

DEFINES += dDOUBLE

SOURCES += main.cpp\
        MainWindow.cpp \
    OpenGLDisplayWidget.cpp \
    Tools/Desenha.cpp \
    Tools/Vetor3D.cpp \
    Tools/Camera.cpp \
    Tools/CameraDistante.cpp \
    Sim/ODESimulation.cpp \
    Sim/Model/Humanoid.cpp \
    Sim/Model/BodyMember.cpp \
    Tools/Utils/Color.cpp \
    Sim/Model/Hand.cpp \
    Sim/Model/Head.cpp \
    Sim/Model/Shoulder.cpp \
    Sim/Model/Elbow.cpp \
    Sim/Model/Hip.cpp \
    Sim/Model/Torso.cpp \
    Sim/Model/Knee.cpp \
    Sim/Model/Foot.cpp \
    Sim/Kinect.cpp

HEADERS  += MainWindow.h \
    OpenGLDisplayWidget.h \
    Tools/Desenha.h \
    Tools/Vetor3D.h \
    Tools/Camera.h \
    Tools/CameraDistante.h \
    Sim/ODESimulation.h \
    Sim/Model/Humanoid.h \
    Sim/Model/BodyMember.h \
    Tools/Utils/Color.h \
    Tools/gVector3D.h \
    Sim/Model/Hand.h \
    Sim/Model/Head.h \
    Sim/Model/Shoulder.h \
    Sim/Model/Elbow.h \
    Sim/Model/Hip.h \
    Sim/Model/Torso.h \
    Sim/Model/Knee.h \
    Sim/Model/Foot.h \
    Sim/Kinect.h \
    Tools/Utils/Structs.h

FORMS    += MainWindow.ui
