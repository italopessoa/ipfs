#ifndef KINECT_H
#define KINECT_H

//#ifndef SAMPLE_XML_PATH_LOCAL
#define SAMPLE_XML_PATH_LOCAL "SamplesConfig.xml"
//#endif SAMPLE_XML_PATH_LOCAL

#include <XnCppWrapper.h>
#include <XnTypes.h>
#include <qdebug.h>
#include <QThread>
#include <QMutex>
#include <QWaitCondition>

class Kinect : public QThread
{
    Q_OBJECT
public:
    explicit Kinect(QObject *parent = 0);
    ~Kinect();

    /**
     * @brief Setup
     */
    void Setup();

    /**
     * @brief Run
     */
  //  void run();

    /**
     * @brief Stop
     */
    void Stop();

    void Pause();

    void Resume();

public slots:
    void on_MaxModelNumberChanged(int max);

signals:

    /**
     * @brief SkeletonChanged
     * @param jointsList
     */
    void SkeletonChanged(QList<XnSkeletonJointTransformation*> *jointsList);

    void NewUserDetected(int userID);

    void UserLost(int userID);

private:


    //http://stackoverflow.com/questions/24484961/emit-a-signal-in-a-static-function
    static Kinect* kinectAux;

    void NewUserDetectedAux(int userID);

    void UserLostAux(int userID);

    int validUsers;

    /**
     * @brief g_Context
     */
    xn::Context g_Context;

    /**
     * @brief g_scriptNode
     */
    xn::ScriptNode g_scriptNode;

    /**
     * @brief g_UserGenerator
     */
    static xn::UserGenerator g_UserGenerator;

    /**
     * @brief status
     */
    static XnStatus status;

    /**
     * @brief usePoseDetection
     */
    static XnBool usePoseDetection;

    /**
     * @brief poseName
     */
    static XnChar poseName[20];// = "PSI";

    int maxNumUsers = 10;
    XnCallbackHandle hUserCallbacks, hCalibrationStart, hCalibrationComplete, hPoseDetected;
    XnSkeletonJointTransformation joint;

    QMutex sync;
    QWaitCondition pauseCond;
    bool execute;

    /**
     * @brief NewUserDetected Callback function called when a new user was detected
     * @param nId
     * @param a
     */
    static void XN_CALLBACK_TYPE NewUserDetected(xn::UserGenerator& , XnUserID userID, void* userPCookie);

    /**
     * @brief LostUser Callback function called when an existing user was lost
     * @param userID
     * @param userPCookie
     */
    static void XN_CALLBACK_TYPE LostUser(xn::UserGenerator& , XnUserID userID, void* userPCookie);

    /**
     * @brief UserPoseDetected Callback function called when a pose is detected
     * @param strPose
     * @param userID
     * @param userPCookie
     */
    static void XN_CALLBACK_TYPE UserPoseDetected(xn::PoseDetectionCapability&, const XnChar* strPose, XnUserID userID, void *userPCookie);

    /**
     * @brief UserCalibrationStart Callback function called when user callibration start
     * @param userID
     * @param userPCookie
     */
    static void XN_CALLBACK_TYPE UserCalibrationStart(xn::SkeletonCapability& , XnUserID userID, void* userPCookie);

    /**
     * @brief UserCalibrationComplete Callback function called when user callibration is completed
     * @param userID
     * @param calibrationStatus
     * @param userPCookie
     */
    static void XN_CALLBACK_TYPE UserCalibrationComplete(xn::SkeletonCapability&, XnUserID userID, XnCalibrationStatus calibrationStatus, void* userPCookie);

    //TODO: adicionar o CHECK_RC
protected:
    void run();
};

//XnCallbackHandle Kinect::hCalibrationComplete= NULL;
/*
xn::UserGenerator Kinect::g_UserGenerator = 0;
XnBool Kinect::status = XN_STATUS_OK;
XnBool Kinect::usePoseDetection = FALSE;
XnChar Kinect::poseName[20] = "lk";

Kinect::Kinect()
{

}

Kinect::~Kinect()
{
    Stop();
}

void XN_CALLBACK_TYPE Kinect::NewUserDetected(xn::UserGenerator& , XnUserID userID, void* userPCookie)
{
    XnUInt32 epochTime = 0;
    xnOSGetEpochTime(&epochTime);
    qDebug()<< epochTime << "Corpo detectado "<< userID;
    printf("%d Corpo perdido %d\n", epochTime, userID);
}

void XN_CALLBACK_TYPE Kinect::LostUser(xn::UserGenerator& , XnUserID userID, void* userPCookie)
{
    XnUInt32 epochTime = 0;
    xnOSGetEpochTime(&epochTime);
    qDebug()<< epochTime << "Corpo perdido "<< userID;
    printf("%d Corpo perdido %d\n", epochTime, userID);
}

void XN_CALLBACK_TYPE Kinect::UserPoseDetected(xn::PoseDetectionCapability&, const XnChar* strPose, XnUserID userID, void *userPCookie)
{
    XnUInt32 epochTime = 0;
    xnOSGetEpochTime(&epochTime);
    qDebug() <<epochTime << " Pose "<<strPose << " para o corpo " << userID;
    printf("%d Pose %s identificada para o corpo %d\n", epochTime, strPose, userID);

    g_UserGenerator.GetPoseDetectionCap().StopPoseDetection(userID);
    g_UserGenerator.GetSkeletonCap().RequestCalibration(userID, TRUE);
}

void XN_CALLBACK_TYPE Kinect::UserCalibrationStart(xn::SkeletonCapability& , XnUserID userID, void* userPCookie)
{
    XnUInt32 epochTime = 0;
    xnOSGetEpochTime(&epochTime);
    qDebug() << epochTime <<" calibragem iniciada para o corpo " << userID;
    printf("%d Calibragem iniciada para o corpo %d\n", epochTime, userID);
}

void XN_CALLBACK_TYPE Kinect::UserCalibrationComplete(xn::SkeletonCapability&, XnUserID userID, XnCalibrationStatus calibrationStatus, void* userPCookie)
{
    XnUInt32 epochTime = 0;
    xnOSGetEpochTime(&epochTime);

    if (status == XN_CALIBRATION_STATUS_OK)
    {
        // Calibration succeeded
        qDebug()<< epochTime <<" calibragem completa, iniciar rastreamento do corpo" << userID;
        printf("%d Calibragem completa, iniciar rastreamento do corpo %d\n", epochTime, userID);
        g_UserGenerator.GetSkeletonCap().StartTracking(userID);
    }
    else
    {
        // Calibration failed
        qWarning()<<epochTime << " Erro ao calibrar corpo " << userID;
        printf("%d Erro ao calibrar corpo %d\n", epochTime, userID);
        if(status==XN_CALIBRATION_STATUS_MANUAL_ABORT)
        {
            printf("Cancelado manualmente, tentativa de calibragem encerrada!");
            return;
        }
        //TODO remove se nao for utilziar pose
        if (usePoseDetection)
        {
            //caso tenha falhado por necessidade de uma posicao, tentar novamente
            g_UserGenerator.GetPoseDetectionCap().StartPoseDetection(poseName, userID);
        }
        else
        {
            //se nao for erro na identificacao da pose, forçar a calibragem novamente
            g_UserGenerator.GetSkeletonCap().RequestCalibration(userID, TRUE);
        }
    }
}

void Kinect::Setup()
{
    xn::EnumerationErrors errors;

    XnBool fileExists;
    xnOSDoesFileExist(SAMPLE_XML_PATH_LOCAL, &fileExists);

    //arquivo que contem as configurações
    const char *fn = NULL;
    if (fileExists) fn = SAMPLE_XML_PATH_LOCAL;
    else {
        //FIXME: adicionar exceção ou enviar mensagem para a class
        qWarning()<< "Não foi possível ler o arquivo de confiruração: " <<SAMPLE_XML_PATH_LOCAL;
        printf("Não foi possível ler o arquivo de confiruração: '%s' . Aborting.\n" , SAMPLE_XML_PATH_LOCAL);
        return;
        //return XN_STATUS_ERROR;
    }
    qDebug()<<"Lendo arquivo de configuração";
    printf("Lendo arquivo de configuração: '%s'\n", fn);

    //inicializar contexto
    status = g_Context.InitFromXmlFile(fn, g_scriptNode, &errors);
    g_Context.SetGlobalMirror(TRUE);
    //se nenhum nó for encontrado
    if (status == XN_STATUS_NO_NODE_PRESENT)
    {
        XnChar strError[1024];
        errors.ToString(strError, 1024);
        printf("%s\n", strError);
        //return (nRetVal);
    }
    else if (status != XN_STATUS_OK)
    {
        qWarning()<< "erro ao ler configurações";
        printf("Erro ao ler configurações: %s\n", xnGetStatusString(status));
        //return (nRetVal);
    }

    status = g_Context.FindExistingNode(XN_NODE_TYPE_USER, g_UserGenerator);
    if (status != XN_STATUS_OK)
    {
        status = g_UserGenerator.Create(g_Context);
        qDebug() << status << " Gerador de usuário encontrado";
        //CHECK_RC(nRetVal, "Gerador de usuário encontrado.");
    }

//    XnCallbackHandle hUserCallbacks, hCalibrationStart, hCalibrationComplete, hPoseDetected;

    //verificar se suporta o skeleton
    //o device pode ser diferente do kinect
    if (!g_UserGenerator.IsCapabilitySupported(XN_CAPABILITY_SKELETON))
    {
        qWarning()<<"O dispositivo não suporta o mapeamento do skeleton";
        printf("O dispositivo não suporta o mapeamento do skeleton\n");
        //return 1;
    }

    //registrar metodo de callback oara identificar um corpo
    g_UserGenerator.RegisterUserCallbacks(NewUserDetected,LostUser, NULL,hUserCallbacks);

    //registrar metodo callback para calibrar skeleton
    status = g_UserGenerator.GetSkeletonCap().RegisterToCalibrationStart(UserCalibrationStart, NULL,hCalibrationStart);
    //CHECK_RC(nRetVal, "Registrar calibrador de corpos (inicio).");

    //registrar metodo de callback para completar a calibragem dos corpos
    status = g_UserGenerator.GetSkeletonCap().RegisterToCalibrationComplete(UserCalibrationComplete, NULL,hCalibrationComplete);
    //CHECK_RC(nRetVal, "Register to calibration complete");

    //verificar se é necessário alguma pose para iniciar a calibragem
    if (g_UserGenerator.GetSkeletonCap().NeedPoseForCalibration())
    {
        usePoseDetection = TRUE;
        //verificar se o dispositivo suporta reconhecimento de poses
        if (!g_UserGenerator.IsCapabilitySupported(XN_CAPABILITY_POSE_DETECTION))
        {
            printf("É necessário uma pose para iniciar a calibragem, mas essa funcionaliade não é suportada.\n");
            //  return 1;
        }

        //registrar metodo callback para identificar uma pose
        status = g_UserGenerator.GetPoseDetectionCap().RegisterToPoseDetected(UserPoseDetected, NULL,hPoseDetected);
        //  CHECK_RC(nRetVal, "Registrar identificador de posilções.");

        //identificar a posição do usuário com base nas posições definidas em g_strPose
        g_UserGenerator.GetSkeletonCap().GetCalibrationPose(poseName);
    }

    //definir quais pontos do skeleton serão identificados
    g_UserGenerator.GetSkeletonCap().SetSkeletonProfile(XN_SKEL_PROFILE_ALL);

    //gerar corpo
    status = g_Context.StartGeneratingAll();
}

void Kinect::run()
{
    XnUserID aUsers[maxNumUsers];
    XnUInt16 nUsers;

    while (!xnOSWasKeyboardHit())
    {
        g_Context.WaitOneUpdateAll(g_UserGenerator);

        // exibir as informações da mão do primeiro corpo calibrado
        //print the torso information for the first user already tracking

        nUsers=maxNumUsers;
        g_UserGenerator.GetUsers(aUsers, nUsers);

        for(XnUInt16 i=0; i<nUsers; i++)
        {
            printf("teste");
        }
    }

}

void Kinect::Stop()
{
    g_scriptNode.Release();
    g_UserGenerator.Release();
    g_Context.Release();
}*/

#endif // KINECT_H
