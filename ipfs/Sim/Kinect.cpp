#include "Kinect.h"
#include <QtCore>
#include <QMessageBox>

xn::UserGenerator Kinect::g_UserGenerator;
XnBool Kinect::status;
XnBool Kinect::usePoseDetection = TRUE;
XnChar Kinect::poseName[20] = "Psi";
Kinect* Kinect::kinectAux = NULL;

#define POSE_TO_USE "Psi"
#define desconected 65540

Kinect::Kinect(QObject *parent) :
    QThread(parent)
{
    kinectAux=this;
}

Kinect::~Kinect()
{
    Stop();
}

void XN_CALLBACK_TYPE Kinect::NewUserDetected(xn::UserGenerator& , XnUserID userID, void* userPCookie)
{
    XnUInt32 epochTime = 0;
    xnOSGetEpochTime(&epochTime);
    qDebug()<< epochTime << "Corpo detectado "<< userID;
    printf("%d Corpo detectado %d\n", epochTime, userID);

    if (usePoseDetection)
    {
        g_UserGenerator.GetPoseDetectionCap().StartPoseDetection(POSE_TO_USE, userID);
    }
    //se nao for necessario, uma requisicao para calibrar o corpo será feita
    else
    {
        g_UserGenerator.GetSkeletonCap().RequestCalibration(userID, TRUE);
        if(kinectAux)
            kinectAux->NewUserDetectedAux(userID);
    }
}

void XN_CALLBACK_TYPE Kinect::LostUser(xn::UserGenerator& , XnUserID userID, void* userPCookie)
{
    XnUInt32 epochTime = 0;
    xnOSGetEpochTime(&epochTime);
    qDebug()<< epochTime << "Corpo perdido "<< userID;
    printf("%d Corpo perdido %d\n", epochTime, userID);

    if(kinectAux)
        kinectAux->UserLostAux(userID);
}

void XN_CALLBACK_TYPE Kinect::UserPoseDetected(xn::PoseDetectionCapability&, const XnChar* strPose, XnUserID userID, void *userPCookie)
{
    XnUInt32 epochTime = 0;
    xnOSGetEpochTime(&epochTime);
    qDebug() <<epochTime << " Pose "<<strPose << " para o corpo " << userID;
    printf("%d Pose %s identificada para o corpo %d\n", epochTime, strPose, userID);

    g_UserGenerator.GetPoseDetectionCap().StopPoseDetection(userID);
    g_UserGenerator.GetSkeletonCap().RequestCalibration(userID, TRUE);
    if(kinectAux)
        kinectAux->NewUserDetectedAux(userID);
}

void XN_CALLBACK_TYPE Kinect::UserCalibrationStart(xn::SkeletonCapability& , XnUserID userID, void* userPCookie)
{
    XnUInt32 epochTime = 0;
    xnOSGetEpochTime(&epochTime);
    qDebug() << epochTime <<" calibragem iniciada para o corpo " << userID;
    printf("%d Calibragem iniciada para o corpo %d\n", epochTime, userID);
}

void XN_CALLBACK_TYPE Kinect::UserCalibrationComplete(xn::SkeletonCapability&, XnUserID userID, XnCalibrationStatus calibrationStatus, void* userPCookie)
{
    XnUInt32 epochTime = 0;
    xnOSGetEpochTime(&epochTime);

    if (status == XN_CALIBRATION_STATUS_OK)
    {
        // Calibration succeeded
        qDebug()<< epochTime <<" calibragem completa, iniciar rastreamento do corpo" << userID;
        printf("%d Calibragem completa, iniciar rastreamento do corpo %d\n", epochTime, userID);
        g_UserGenerator.GetSkeletonCap().StartTracking(userID);
    }
    else
    {
        // Calibration failed
        qWarning()<<epochTime << " Erro ao calibrar corpo " << userID;
        printf("%d Erro ao calibrar corpo %d\n", epochTime, userID);
        if(status==XN_CALIBRATION_STATUS_MANUAL_ABORT)
        {
            printf("Cancelado manualmente, tentativa de calibragem encerrada!");
            return;
        }
        //TODO remove se nao for utilziar pose
        if (usePoseDetection)
        {
            //caso tenha falhado por necessidade de uma posicao, tentar novamente
            g_UserGenerator.GetPoseDetectionCap().StartPoseDetection(POSE_TO_USE, userID);
        }
        else
        {
            //se nao for erro na identificacao da pose, forçar a calibragem novamente
            g_UserGenerator.GetSkeletonCap().RequestCalibration(userID, TRUE);
        }
    }
}

void Kinect::Setup()
{
    xn::EnumerationErrors errors;

    XnBool fileExists;
    xnOSDoesFileExist(SAMPLE_XML_PATH_LOCAL, &fileExists);

    //arquivo que contem as configurações
    const char *fn = NULL;
    if (fileExists) fn = SAMPLE_XML_PATH_LOCAL;
    else {
        //FIXME: adicionar exceção ou enviar mensagem para a class
        qWarning()<< "Não foi possível ler o arquivo de confiruração: " <<SAMPLE_XML_PATH_LOCAL;
        printf("Não foi possível ler o arquivo de confiruração: '%s' . Aborting.\n" , SAMPLE_XML_PATH_LOCAL);
        return;
        //return XN_STATUS_ERROR;
    }
    qDebug()<<"Lendo arquivo de configuração";
    printf("Lendo arquivo de configuração: '%s'\n", fn);

    //inicializar contexto
    status = g_Context.InitFromXmlFile(fn, g_scriptNode, &errors);
    g_Context.SetGlobalMirror(TRUE);
    //se nenhum nó for encontrado
    if (status == XN_STATUS_NO_NODE_PRESENT)
    {
        XnChar strError[1024];
        errors.ToString(strError, 1024);
        printf("%s\n", strError);
        //return (nRetVal);
    }
    else if (status != XN_STATUS_OK)
    {
        qWarning()<< "erro ao ler configurações";
        printf("Erro ao ler configurações: %s\n", xnGetStatusString(status));
        //return (nRetVal);
    }

    status = g_Context.FindExistingNode(XN_NODE_TYPE_USER, g_UserGenerator);
    if (status != XN_STATUS_OK)
    {
        status = g_UserGenerator.Create(g_Context);
        if(status == desconected)
        {
        //TODO: adicionar mensagem de erro e enviar para a janela
            qDebug() << "DESCONECTADO";
        }
        qDebug() << status << " Gerador de usuário encontrado";
        //CHECK_RC(nRetVal, "Gerador de usuário encontrado.");
    }

    //    XnCallbackHandle hUserCallbacks, hCalibrationStart, hCalibrationComplete, hPoseDetected;

    //verificar se suporta o skeleton
    //o device pode ser diferente do kinect
    if(!g_UserGenerator.IsValid())
    {
        qWarning()<<"123123123";
        printf("234234243\n");
    }
    if (!g_UserGenerator.IsCapabilitySupported(XN_CAPABILITY_SKELETON))
    {
        qWarning()<<"O dispositivo não suporta o mapeamento do skeleton";
        printf("O dispositivo não suporta o mapeamento do skeleton\n");
        //return 1;
    }

    //registrar metodo de callback oara identificar um corpo
    g_UserGenerator.RegisterUserCallbacks(NewUserDetected,LostUser, NULL,hUserCallbacks);

    g_UserGenerator.GetPoseDetectionCap().RegisterToPoseCallbacks(UserPoseDetected, NULL, NULL, hPoseDetected);

    //registrar metodo callback para calibrar skeleton
    status = g_UserGenerator.GetSkeletonCap().RegisterToCalibrationStart(UserCalibrationStart, NULL,hCalibrationStart);
    //CHECK_RC(nRetVal, "Registrar calibrador de corpos (inicio).");

    //registrar metodo de callback para completar a calibragem dos corpos
    status = g_UserGenerator.GetSkeletonCap().RegisterToCalibrationComplete(UserCalibrationComplete, NULL,hCalibrationComplete);
    //CHECK_RC(nRetVal, "Register to calibration complete");

    //verificar se é necessário alguma pose para iniciar a calibragem
    //qDebug()<<"POSE: "<<g_UserGenerator.GetSkeletonCap().NeedPoseForCalibration();
   /*if (g_UserGenerator.GetSkeletonCap().NeedPoseForCalibration())
    {
        qDebug()<< "detecao de pose";
        usePoseDetection = TRUE;
        //verificar se o dispositivo suporta reconhecimento de poses
        if (!g_UserGenerator.IsCapabilitySupported(XN_CAPABILITY_POSE_DETECTION))
        {
            qDebug()<<"É necessário uma pose para iniciar a calibragem, mas essa funcionaliade não é suportada.";
            printf("É necessário uma pose para iniciar a calibragem, mas essa funcionaliade não é suportada.\n");
            //  return 1;
        }

        //registrar metodo callback para identificar uma pose
        status = g_UserGenerator.GetPoseDetectionCap().RegisterToPoseDetected(UserPoseDetected, NULL,hPoseDetected);
        //  CHECK_RC(nRetVal, "Registrar identificador de posilções.");

        //identificar a posição do usuário com base nas posições definidas em g_strPose
        g_UserGenerator.GetSkeletonCap().GetCalibrationPose(POSE_TO_USE);
    }*/

    //definir quais pontos do skeleton serão identificados
    g_UserGenerator.GetSkeletonCap().SetSkeletonProfile(XN_SKEL_PROFILE_ALL);

    //gerar corpo
    status = g_Context.StartGeneratingAll();
}

void Kinect::Stop()
{
    sync.lock();
    execute = false;
    g_scriptNode.Release();
    g_UserGenerator.Release();
    g_Context.Release();
    sync.unlock();
}

void Kinect::Pause()
{
    sync.lock();
    execute = false;
    g_Context.StopGeneratingAll();
    sync.unlock();
}

void Kinect::Resume()
{
    sync.lock();
    execute = true;
    g_Context.StartGeneratingAll();
    sync.unlock();
    pauseCond.wakeAll();
}

void Kinect::run()
{
    XnUserID aUsers[maxNumUsers];
    XnUInt16 nUsers;
    execute = true;
    while (true)
    {
        if (execute && validUsers >= 0)
        {
            //qDebug() << "rodando";
            sync.lock();
            g_Context.WaitOneUpdateAll(g_UserGenerator);

            // exibir as informações da mão do primeiro corpo calibrado
            //print the torso information for the first user already tracking

            nUsers=maxNumUsers;
            g_UserGenerator.GetUsers(aUsers, nUsers);

            QList<XnSkeletonJointTransformation*> *jointsList = new QList<XnSkeletonJointTransformation*>();

            for(XnUInt16 i=0; (i<nUsers && i<validUsers); i++)
            {
                XnSkeletonJointTransformation joints[25];

                //qDebug() << g_UserGenerator.GetSkeletonCap().IsTracking(aUsers[i]);
                if(g_UserGenerator.GetSkeletonCap().IsTracking(aUsers[i]) == FALSE)
                    continue;

                //TODO: adicionar pulso e tornozelo
                g_UserGenerator.GetSkeletonCap().GetSkeletonJoint(aUsers[i],XN_SKEL_HEAD,joints[XN_SKEL_HEAD]);
                g_UserGenerator.GetSkeletonCap().GetSkeletonJoint(aUsers[i],XN_SKEL_NECK,joints[XN_SKEL_NECK]);

                g_UserGenerator.GetSkeletonCap().GetSkeletonJoint(aUsers[i],XN_SKEL_LEFT_SHOULDER,joints[XN_SKEL_LEFT_SHOULDER]);
                g_UserGenerator.GetSkeletonCap().GetSkeletonJoint(aUsers[i],XN_SKEL_RIGHT_SHOULDER,joints[XN_SKEL_RIGHT_SHOULDER]);

                g_UserGenerator.GetSkeletonCap().GetSkeletonJoint(aUsers[i],XN_SKEL_LEFT_ELBOW,joints[XN_SKEL_LEFT_ELBOW]);
                g_UserGenerator.GetSkeletonCap().GetSkeletonJoint(aUsers[i],XN_SKEL_RIGHT_ELBOW,joints[XN_SKEL_RIGHT_ELBOW]);

                g_UserGenerator.GetSkeletonCap().GetSkeletonJoint(aUsers[i],XN_SKEL_LEFT_HAND,joints[XN_SKEL_LEFT_HAND]);
                g_UserGenerator.GetSkeletonCap().GetSkeletonJoint(aUsers[i],XN_SKEL_RIGHT_HAND,joints[XN_SKEL_RIGHT_HAND]);

                g_UserGenerator.GetSkeletonCap().GetSkeletonJoint(aUsers[i],XN_SKEL_TORSO,joints[XN_SKEL_TORSO]);

                g_UserGenerator.GetSkeletonCap().GetSkeletonJoint(aUsers[i],XN_SKEL_LEFT_HIP,joints[XN_SKEL_LEFT_HIP]);
                g_UserGenerator.GetSkeletonCap().GetSkeletonJoint(aUsers[i],XN_SKEL_RIGHT_HIP,joints[XN_SKEL_RIGHT_HIP]);

                g_UserGenerator.GetSkeletonCap().GetSkeletonJoint(aUsers[i],XN_SKEL_LEFT_KNEE,joints[XN_SKEL_LEFT_KNEE]);
                g_UserGenerator.GetSkeletonCap().GetSkeletonJoint(aUsers[i],XN_SKEL_RIGHT_KNEE,joints[XN_SKEL_RIGHT_KNEE]);

                g_UserGenerator.GetSkeletonCap().GetSkeletonJoint(aUsers[i],XN_SKEL_LEFT_FOOT,joints[XN_SKEL_LEFT_FOOT]);
                g_UserGenerator.GetSkeletonCap().GetSkeletonJoint(aUsers[i],XN_SKEL_RIGHT_FOOT,joints[XN_SKEL_RIGHT_FOOT]);

                jointsList->append(joints);

                //qDebug() <<j.position.position.X;// <<", " <<j.position.position.Y <<", "<<j.position.position.Z;
            }
            emit SkeletonChanged(jointsList);

            sync.unlock();
        }
    }

}

void Kinect::NewUserDetectedAux(int userID)
{
    emit NewUserDetected(userID);
}

void Kinect::UserLostAux(int userID)
{
    emit UserLost(userID);
}

void Kinect::on_MaxModelNumberChanged(int max)
{
    validUsers = max;
}
