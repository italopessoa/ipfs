#ifndef ODESIMULATION_H
#define ODESIMULATION_H
#include <ode/ode.h>
#include "Tools/gVector3D.h"
#include <qobject.h>
#include <XnTypes.h>
#include "Model/Humanoid.h"
#include "Tools/Utils/Structs.h"

class ODESimulation : public QObject
{
    Q_OBJECT
public:
    /**
     * @brief ODESimulation default constructor
     */
    ODESimulation();

    /**
     * @brief ODESimulation constructor to set a new step value
     * @param wordlStep step value
     */
    ODESimulation(dReal wordlStep);

    /**
     * @brief ODESimulation constructor to set a new gravity value
     * @param gravity gravity value
     */
    explicit ODESimulation(gVector3D<dReal> gravity);

    /**
     * @brief ODESimulation constructor to set step and gravity values
     * @param wordlStep step value
     * @param gravity gravity value
     */
    explicit ODESimulation(dReal wordlStep,gVector3D<dReal> gravity);

    ~ODESimulation();

    /**
     * @brief Start create the new world with default values
     */
    void Start();

    /**
     * @brief Stop stop the simulation
     */
    void Stop();

    /**
     * @brief Destroy destroy all the simulation
     * @see ~ODESimulation()
     */
    void Destroy();

    /**
     * @brief Restart restar the simulations's world
     */
    void Restart();

    /**
     * @brief setGravity change the gravity
     * @param gravity gravity vector
     */
    void setGravity(gVector3D<dReal> gravity);

    /**
     * @brief setWorldStep change worldStep
     * @param worldStep step value
     */
    void setWorldStep(dReal worldStep);

    /**
     * @brief ExecuteLoop start the physics simulation loop
     */
    void ExecuteLoop();

    /**
     * @brief getSpace get the actual world's space
     * @return actual world id
     */
    dSpaceID getSpace();

    gVector3D<dReal> testeGetPosition();


public slots:
    /**
     * @brief onSkeletonChanged
     * @param jointsList
     */
    void onSkeletonChanged(QList<XnSkeletonJointTransformation *> *jointsList);

    void on_humanoid_calibrated(SkeletonJointPosition *skeletonJoints, int modelID);

    void on_MaxModelsNumber_Changed(int number);

    void on_InsertModel(int modelID);

private:
    /**
     * @brief worldStep world step value
     */
    dReal worldStep;

    /**
     * @brief world actual world's id
     */
    static dWorldID world;

    /**
     * @brief space actual world's space id
     */
    dSpaceID space;

    /**
     * @brief gravity world gravity value
     */
    gVector3D<dReal> gravity;

    static dGeomID ground;

    //QList<Humanoid*> *models;

    /**
     * @brief nearCallback detect collision between geoms attached
     * @param data
     * @param object1
     * @param object2
     */
    static void nearCallback(void *data, dGeomID object1, dGeomID object2);

    static dJointGroupID jointsContactGroup;

    int MAX_MODELS;

    Humanoid *models;
    Humanoid *calibratedModels;

    dReal kx_MAX;
    dReal kx_MIN;
    dReal ky_MAX;
    dReal ky_MIN;
    dReal kz_MAX;
    dReal kz_MIN;

    dReal ox_MAX;
    dReal ox_MIN;
    dReal oy_MAX;
    dReal oy_MIN;
    dReal oz_MAX;
    dReal oz_MIN;

    dBodyID testeBody;
    dGeomID testeGeom;
    dMass m1;
    dReal mass = 10;
};

#endif // ODESIMULATION_H
