#ifndef HUMANOID_H
#define HUMANOID_H

#include "Elbow.h"
#include "Shoulder.h"
#include <XnTypes.h>
#include "Tools/Utils/Structs.h"

class Humanoid
{
public:
    Humanoid();
    Humanoid(dWorldID world, dSpaceID space);
    ~Humanoid();

    void CreateElbowR(SkeletonJointPosition skeletonJoint);
    Elbow *getRightElbow();

    void Draw();

    void Activade();

    void Disactivate();

    bool IsActive();

private:
    Elbow *rElbow;

    Shoulder *rShoulder;

    dWorldID world;

    dSpaceID space;

    bool isActive;
};

#endif // HUMANOID_H
