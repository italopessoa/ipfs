#ifndef KNEE_H
#define KNEE_H

#include "BodyMember.h"

class Knee : public BodyMember
{
public:
    Knee();
    ~Knee();
};

#endif // KNEE_H
