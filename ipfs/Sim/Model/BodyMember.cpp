#include "BodyMember.h"

BodyMember::BodyMember()
{

}

BodyMember::BodyMember(dWorldID world, dSpaceID space)
{
    this->world = world;
    this->space = space;
}

BodyMember::BodyMember(dBodyID bodyID,dGeomID geomID)
{
    this->bodyID = bodyID;
    this->geomID = geomID;
}

BodyMember::BodyMember(dBodyID bodyID,dGeomID geomID, Color color)
{
    this->bodyID = bodyID;
    this->geomID = geomID;
    this->color = color;
}

BodyMember::~BodyMember()
{

}

dBodyID BodyMember::getBodyID()
{
    return this->bodyID;
}

dGeomID BodyMember::getGeomID()
{
    return this->geomID;
}

void BodyMember::setColor(Color color)
{
    this->color = color;
}

Color BodyMember::getColor()
{
    return this->color;
}

void BodyMember::setDesiredPosition(gVector3D<dReal> position)
{
    this->desiredPosition = position;
}

gVector3D<dReal> BodyMember::getDesiredPosition()
{
    return this->desiredPosition;
}
//FIXME: corrigir precisao
dReal* BodyMember::KinectToODEMatrix(float* elements)
{
    dMatrix3 rot;
    rot[0] = elements[0];
    rot[1] = elements[1];
    rot[2] = elements[2];

    rot[4] = elements[3];
    rot[5] = elements[4];
    rot[6] = elements[5];

    rot[8] = elements[6];
    rot[9] = elements[7];
    rot[10] = elements[8];
    rot[3] = rot[7] = rot[11] = 0;

    return rot;
}

void BodyMember::setdGeomID(dGeomID geomID)
{
    this->geomID = geomID;
}

void BodyMember::KinectToODEVector(gVector3D<dReal> &kVector)
{
    dReal kx_max = 50;
    dReal kx_min = -50;
    dReal ky_max = 50;
    dReal ky_min = -50;
    dReal kz_max = 300;
    dReal kz_min = 200;

    dReal ox_max = 1;
    dReal ox_min = -1;
    dReal oy_max = 22;
    dReal oy_min = 20;
    dReal oz_max = 0;
    dReal oz_min = 2;

    kVector.x = ox_min + ( (kVector.x-kx_min)/(kx_max-kx_min) )*(ox_max-ox_min);
    kVector.y = oy_min + ( (kVector.y-ky_min)/(ky_max-ky_min) )*(oy_max-oy_min);
    kVector.z = oz_min + ( (kVector.z-kz_min)/(kz_max-kz_min) )*(oz_max-oz_min);
}
