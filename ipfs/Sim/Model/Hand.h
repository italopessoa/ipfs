#ifndef HAND_H
#define HAND_H

#include "BodyMember.h"

class Hand : public BodyMember
{
public:
    Hand();
    ~Hand();
};

#endif // HAND_H
