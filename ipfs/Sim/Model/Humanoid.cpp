#include "Humanoid.h"
#include <XnTypes.h>
#include <ode/ode.h>

Humanoid::Humanoid()
{

}

Humanoid::Humanoid(dWorldID world, dSpaceID space)
{
    this->world = world;
    this->space = space;

    isActive = false;
}

Humanoid::~Humanoid()
{

}

void Humanoid::CreateElbowR(SkeletonJointPosition skeletonJoint)
{
    //a (1,2)
    //b (3,4)
    /*XnVector3D a = skeletonJoint.position;
    XnVector3D b = skeletonJoint.parentPosition;

    dReal length = sqrt(pow(a.X-b.X,2) + pow(a.Y-b.Y,2) + pow(a.Z-b.Z,2));*/

    //this->rElbow = new Elbow(this->world,skeletonJoint);//dBodyCreate(this->world),dCreateCapsule(this->space,0.3,length));
    //this->rElbow->setDesiredPosition(gVector3D<dReal>(a.X,a.Y,a.Z));

    rShoulder = new Shoulder(this->world,this->space,skeletonJoint);
}

Elbow* Humanoid::getRightElbow()
{
    return this->rElbow;
}

void Humanoid::Draw()
{
    rShoulder->Draw();
}

void Humanoid::Activade()
{
    this->isActive = true;
}

void Humanoid::Disactivate()
{
    this->isActive = false;
}

bool Humanoid::IsActive()
{
    return this->isActive;
}
