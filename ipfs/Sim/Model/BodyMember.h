#ifndef BODYMEMBER_H

#include <ode/common.h>
#include <Tools/Utils/Color.h>
#include <GL/gl.h>
#include <Tools/gVector3D.h>
#include "Tools/Utils/Structs.h"
#include <ode/ode.h>

class BodyMember
{
public:
     BodyMember();
    /**
     * @brief BodyMember default constructor
     */
    BodyMember(dWorldID world, dSpaceID space);

    /**
     * @brief BodyMember constructor to set a new body and geom
     * @param bodyID body id
     * @param geomID geom id
     */
    BodyMember(dBodyID bodyID,dGeomID geomID);

    /**
     * @brief BodyMember constructor to set a body with customized color
     * @param bodyID body id
     * @param geomID geom id
     * @param color body color
     */
    BodyMember(dBodyID bodyID,dGeomID geomID, Color color);

    ~BodyMember();

    /**
     * @brief getBodyID get body id
     * @return dBodyID value
     */
    dBodyID getBodyID();

    /**
     * @brief getGeomID get body geom id
     * @return dGeomID value
     */
    dGeomID getGeomID();

    /**
     * @brief setColor changes the body color
     * @param color body color value
     */
    void setColor(Color color);

    /**
     * @brief getColor get the body color
     * @return Color object
     */
    Color getColor();

    /**
     * @brief setDesiredPosition updates the desired position
     * @param position desired position to the body
     */
    void setDesiredPosition(gVector3D<dReal> position);

    /**
     * @brief getDesiredPosition get body's desired position
     * @return gVector3D<dReal> desired position
     */
    gVector3D<dReal> getDesiredPosition();

    /**
     * @brief Draw
     */
    virtual void Draw() = 0;

private:

protected:

    /**
     * @brief bodyID body id
     */
    dBodyID bodyID;
    /**
     * @brief geomID body's geom id
     */
    dGeomID geomID;

    /**
     * @brief child child member
     */
    BodyMember *child;

    /**
     * @brief color body color
     */
    Color color;

    /**
     * @brief desiredPosition desired position value
     */
    gVector3D<dReal> desiredPosition;

    dWorldID world;

    dSpaceID space;

    dReal* KinectToODEMatrix(float* elements);

    void setdGeomID(dGeomID geomID);

    void KinectToODEVector(gVector3D<dReal> &kVector);
};

#define BODYMEMBER_H
#endif // BODYMEMBER_H
