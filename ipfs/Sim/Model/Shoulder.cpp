#include "Shoulder.h"
#include <GL/glu.h>
#include "Tools/Desenha.h"
#include <QDebug>
Shoulder::Shoulder(dWorldID world, dSpaceID space, SkeletonJointPosition skeletonJoint) :
    BodyMember(world,space)
{
    KinectToODEVector(skeletonJoint.position);
    KinectToODEVector(skeletonJoint.childPosition);

    this->bodyID = dBodyCreate(this->world);
    setdGeomID(dCreateCapsule(this->space,1,skeletonJoint.position.getDistance(skeletonJoint.childPosition)));
    dGeomSetBody(this->geomID,this->bodyID);
    dBodySetPosition(this->bodyID,skeletonJoint.position.x,skeletonJoint.position.y,skeletonJoint.position.z);
    //dBodySetRotation(this->bodyID,KinectToODEMatrix(skeletonJoint.orientation.elements));

    //setDesiredPosition(skeletonJoint.position);

    dQuaternion q;
    dRtoQ(KinectToODEMatrix(skeletonJoint.orientation.elements),q);
    //qDebug() << q[0] << " : " << skeletonJoint.orientation.elements[0];
    //depois 0.643779  :  0.657804 antes

    dBodySetQuaternion(this->bodyID,q);
}

Shoulder::~Shoulder()
{

}

void Shoulder::Draw()
{
    //qDebug() <<"Y: " << dBodyGetPosition(bodyID)[1];
    glPushMatrix();
        GLdouble rotation[16];
        const dReal *m2 = dBodyGetRotation(this->bodyID);

        rotation[0] = m2[0];
        rotation[1] = m2[1];
        rotation[2] = m2[2];
        rotation[3] = m2[3];
        rotation[4] = m2[4];
        rotation[5] = m2[5];
        rotation[6] = m2[6];
        rotation[7] = m2[7];
        rotation[8] = m2[8];
        rotation[9] = m2[9];
        rotation[10] = m2[10];
        rotation[11] = m2[11];
        rotation[12] = 0;
        rotation[13] = 0;
        rotation[14] = 0;
        rotation[15] = 1;

        //dMatrix3 R;
        //dRFromEulerAngles (R,grauToRad(90.0),grauToRad(0.0),grauToRad(0.0));
        glMultTransposeMatrixd(rotation);

        glColor3d(1, 0, 0);
        //glRotated(90,0,1,0);
        GLUquadric *qua = gluNewQuadric();

        dReal radius;
        dReal length;
        dGeomCapsuleGetParams(this->geomID,&radius,&length);

        Desenha::gluCapsule(qua,radius,radius,length,10,10);
        gluDeleteQuadric(qua);

    glPopMatrix();
}
