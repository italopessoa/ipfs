#ifndef HEAD_H
#define HEAD_H

#include "BodyMember.h"

class Head : public BodyMember
{
public:
    Head();
    ~Head();
};

#endif // HEAD_H
