#ifndef FOOT_H
#define FOOT_H

#include "BodyMember.h"

class Foot : public BodyMember
{
public:
    Foot();
    ~Foot();
};

#endif // FOOT_H
