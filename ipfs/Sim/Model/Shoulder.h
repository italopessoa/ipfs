#ifndef SHOULDER_H
#define SHOULDER_H

#include "BodyMember.h"

class Shoulder : public BodyMember
{
public:
    Shoulder(dWorldID world, dSpaceID space, SkeletonJointPosition skeletonJoint);
    ~Shoulder();

    void Draw();

private:
    //FIXME: acho que isso vai ser removido
    //foi apenas um teste do desenho
    SkeletonJointPosition skeletonJoint;
};

#endif // SHOULDER_H
