#ifndef TORSO_H
#define TORSO_H

#include "BodyMember.h"

class Torso : public BodyMember
{
public:
    Torso();
    ~Torso();
};

#endif // TORSO_H
