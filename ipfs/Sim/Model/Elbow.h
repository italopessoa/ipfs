#ifndef ELBOW_H
#define ELBOW_H

#include "BodyMember.h"

class Elbow : public BodyMember
{
public:
    Elbow(dWorldID world, dSpaceID space, SkeletonJointPosition skeletonJoint);
    ~Elbow();

    void Draw();
};

#endif // ELBOW_H
