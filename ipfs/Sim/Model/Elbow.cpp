#include "Elbow.h"

Elbow::Elbow(dWorldID world,dSpaceID space,SkeletonJointPosition skeletonJoint) :
    BodyMember(world,space)
{
    this->bodyID = dBodyCreate(this->world);
    setDesiredPosition(skeletonJoint.position);
}

Elbow::~Elbow()
{

}

void Elbow::Draw()
{

}

