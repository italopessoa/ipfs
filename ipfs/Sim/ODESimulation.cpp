#include "ODESimulation.h"
#include <GL/glut.h>
#include <stdexcept>
#include <QDebug>
#include <XnTypes.h>
#include <math.h>
#include <iterator>
#include <QMutex>

dJointGroupID ODESimulation::jointsContactGroup  = 0;
dGeomID ODESimulation::ground = 0;
dWorldID ODESimulation::world = NULL;

ODESimulation::ODESimulation()
{
    this->gravity = gVector3D<dReal>(0.0,-9.81,0.0);
    this->worldStep = 0.01;

    //this->models = new QList<Humanoid*>();
    MAX_MODELS = 0;
}


ODESimulation::ODESimulation(dReal wordlStep)
{
    this->worldStep = wordlStep;
    //this->models = new QList<Humanoid*>();
    MAX_MODELS = 0;
}

ODESimulation::ODESimulation(gVector3D<dReal> gravity)
{
    this->gravity = gravity;
    //this->models = new QList<Humanoid*>();
}

ODESimulation::ODESimulation(dReal wordlStep, gVector3D<dReal> gravity)
{
    this->worldStep = wordlStep;
    this->gravity = gravity;
    //this->models = new QList<Humanoid*>();
}

ODESimulation::~ODESimulation()
{
    this->Stop();
}

void ODESimulation::Start()
{
    dInitODE();
    this->world = dWorldCreate();
    this->space = dHashSpaceCreate (0);

    dWorldSetGravity (world,gravity.x,gravity.y,gravity.z);
    dWorldSetERP (world,0.2); //juntas ligadas - >ERP:+ligadas
    dWorldSetCFM (world,1e-009); //soft hard (colisao)- >CFM:+soft
    dWorldSetAutoDisableFlag (world,1);
    dWorldSetContactMaxCorrectingVel (world,0.3);
    dWorldSetContactSurfaceLayer (world,0.00001);

    testeBody = dBodyCreate(this->world);
    testeGeom = dCreateBox(this->space,1,1,1);// Geom()
    dGeomSetBody (testeGeom,testeBody);
    dMassSetZero(&m1);
    dMassSetBoxTotal(&m1,mass,1,1,1);
    dBodySetMass(testeBody,&m1);
    dBodySetPosition(testeBody,10,10,10);


    ground = dCreatePlane(space,0,1,0,0);
    jointsContactGroup = dJointGroupCreate(0);
}

void ODESimulation::Stop()
{
    this->Destroy();;
    //dWorldDestroy(this->world);
}

gVector3D<dReal> ODESimulation::testeGetPosition()
{
    const dReal *v = dBodyGetPosition(testeBody);
    return gVector3D<dReal>(v[0],v[1],v[2]);
}

void ODESimulation::Destroy()
{

}

void ODESimulation::Restart()
{

}

void ODESimulation::ExecuteLoop()
{
    if(world == NULL)
    {
        throw std::invalid_argument("ODESimulation::ExecuteLoop() the world does not exist. Call ODESimulation::Start() before execute loop." );
    }

    dSpaceCollide(space,0,&nearCallback);
    dWorldStep (world,worldStep);
    dJointGroupEmpty(jointsContactGroup);
    //dWorldSetGravity (world,gravity.x,gravity.y,gravity.z);

    glPushMatrix();
        const dReal *v = dBodyGetPosition(testeBody);
        glTranslated(v[0],v[1],v[2]);
        glColor3d(1,0,0);
        glutSolidCube(1);

        //qDebug()<<v[1];
    glPopMatrix();

    QMutex q;
    q.lock();
    glPushMatrix();
        for (int i = 0; i < MAX_MODELS; i++)
        {
            if(models[i].IsActive())
            {
                models[i].Draw();
            }
        }
    glPopMatrix();
    q.unlock();
}

void ODESimulation::setGravity(gVector3D<dReal> gravity)
{
    this->gravity = gravity;
}

void ODESimulation::setWorldStep(dReal worldStep)
{
    this->worldStep = worldStep;
}

dSpaceID ODESimulation::getSpace()
{
    return this->space;
}

void ODESimulation::nearCallback(void *data, dGeomID object1, dGeomID object2)
{
    const int N = 10;
    dContact contact[N];

    int isGround = ((ground == object1) || (ground == object2));

    int n = dCollide(object1,object2,N,&contact[0].geom,sizeof(dContact));

    //if (isGround)  {
    if (n >= 1) {
        //qWarning()<<"CONTATO";
    }
    if (isGround)  {
        //if (n >= 1) qDebug() << "flag = 1";
       // else qDebug()<< "flag = 0";
        for (int i = 0; i < n; i++) {
            contact[i].surface.mode = dContactBounce;
            contact[i].surface.mu   = 100;
            contact[i].surface.bounce     = 0.1; // (0.0~1.0) restitution parameter
            contact[i].surface.bounce_vel = 0.0; // minimum incoming velocity for bounce
            dJointID c = dJointCreateContact(world,jointsContactGroup,&contact[i]);
            dJointAttach (c,dGeomGetBody(contact[i].geom.g1),dGeomGetBody(contact[i].geom.g2));

            //pontos de contato
            glPushMatrix();
                glTranslatef(contact[i].geom.pos[0],contact[i].geom.pos[1],contact[i].geom.pos[2]);
                GLUquadricObj *quadrico = gluNewQuadric();
                gluSphere(quadrico,0.01,20,20);
                gluDeleteQuadric(quadrico);
            glPopMatrix();

        }
    }
}

void ODESimulation::on_humanoid_calibrated(SkeletonJointPosition *skeletonJoints, int modelID)
{
    //Humanoid *h = new Humanoid(world,space);
    Humanoid h(world,space);
    h.CreateElbowR(skeletonJoints[XN_SKEL_RIGHT_SHOULDER]);
    calibratedModels[modelID] = h;
    //h->getRightElbow()->getDesiredPosition();
    //return sqrt(pow(a[0]-b[0],2) + pow(a[1]-b[1],2) + pow(a[2]-b[2],2));
}

void ODESimulation::onSkeletonChanged(QList<XnSkeletonJointTransformation*> *jointsList)
{
    QListIterator<XnSkeletonJointTransformation*> iterator(*jointsList);
    //qDebug() << "CAPTURANDO...**********************************";
    /*while(iterator.hasNext())
    {
        qDebug()<<iterator.next()[XN_SKEL_HEAD].position.position.X;
    }*/
    //qWarning() << "*************CHANGED**************";
}

void ODESimulation::on_MaxModelsNumber_Changed(int number)
{
    QMutex m;
    m.lock();
    if(MAX_MODELS > 0)
    {
        if(number > 0)
        {
            Humanoid *modelsTemp = new Humanoid[number];
            Humanoid *calibratedModelsTemp = new Humanoid[number];

            //FIXME: tentar utilizar alguma funcao como memcopy

            //copia valores atuais
            for (int i = 0; i < MAX_MODELS; i++)
            {
                modelsTemp[i] = models[i];
                calibratedModelsTemp[i] = calibratedModels[i];
            }

            //atualiza quantidade de modelos
            MAX_MODELS = number;

            //redimensiona vetores
            //TODO: verificar se isso vai deixar lixo na memoria
            models = new Humanoid[MAX_MODELS];
            calibratedModels = new Humanoid[MAX_MODELS];

            //copia valores para o vetor atual
            for (int i = 0; i < MAX_MODELS; i++)
            {
                models[i] = modelsTemp[i];
                calibratedModels[i] = calibratedModelsTemp[i];
            }

            //delete modelsTemp;
            //delete calibratedModelsTemp;
        }
    }
    else
    {
        //aqui ainda nao existe nenhum elemento
        if(number > 0)
        {
            models = new Humanoid[number];
            models[0].Disactivate();
            qDebug() << models[0].IsActive();
            calibratedModels = new Humanoid[number];
            calibratedModels[0].Disactivate();
            MAX_MODELS = number;
        }
    }
    m.unlock();
}

void ODESimulation::on_InsertModel(int modelID)
{
    models[modelID] = calibratedModels[modelID];
    models->Activade();
}
