#include "MainWindow.h"
#include <QApplication>
#include <QDesktopWidget>
#include <GL/glut.h>
#include <qdebug.h>
#include <Tools/Vetor3D.h>
#include <Tools/gVector3D.h>
int main(int argc, char *argv[])
{

    gVector3D<GLfloat> asd = gVector3D<GLfloat>(1,2,3);
    asd.add(gVector3D<GLfloat>(1,2,3));
    asd.multiplicacao(10);

    Vetor3D asd2 = Vetor3D(1,2,3);
    asd2.add(Vetor3D(1,2,3));
    asd2.multiplicacao(10);

    /*Vetor3D as;
    as.x=as.y=as.z=10;
    Vetor3D f = as*(3);
    qDebug()<<f.x;*/
    glutInit(&argc, argv);
    QApplication a(argc, argv);
    MainWindow window;
    window.resize(window.sizeHint());
    int desktopArea = QApplication::desktop()->width() *
                    QApplication::desktop()->height();
    int widgetArea = window.width() * window.height();

    window.setWindowTitle("Simulation");

    if (((float)widgetArea / (float)desktopArea) < 0.75f)
       window.show();
    else
       window.showMaximized();

    return a.exec();
}
